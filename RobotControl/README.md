# How to make this application run

## Pre-Requiements

* NodeJS installed on system [NodeJs Download Page](https://nodejs.org/en/download/)
* Ionic FrameWork and Cordova installed

```bash
$ npm install -g ionic cordova
```

## Run Application with the Ionic CLI:

with command line enter on project directory and follow this steps

```bash
$ npm install
```

### Run Application to use on browsers

```bash
$ ionic serve
```

### Run native Application from Android

Before you can do this commands you need to install Android SDK on your system first.

```bash
$ ionic cordova platform add android
$ ionic cordova run android
```

### Run native Application From IOS

Only on a Apple computer system running MacOS it´s possible to build native IOS application, you need Xcode and Command Line Tools installed on your system before proceed with this commands.
You need to have developer account too.

1. Add ios platform to the project

```bash
$ ionic cordova platform add ios
```

2. Go to project folder and enter on platforms/ios.

3. Open MyApp.xcworkspace in xcode.

4. Select project name, change Bundle Identifier to your apple id name and change Team to your team, if you don't have this configured follow [this guide](https://developer.apple.com/library/content/documentation/IDEs/Conceptual/AppStoreDistributionTutorial/AddingYourAccounttoXcode/AddingYourAccounttoXcode.html). ![xcodeConfig.png](xcodeConfig.png)

5. Select project tree and add a method to classes/AppDelegate.m, this is to make app accept all ssl certificates, this is needed to get image from robot, because this project uses self certification. ![sslConfig.png](sslConfig.png)

6. Select play button, if you get error, follow [this guide](https://ionicframework.com/docs/intro/deploying/#trusting-the-certificate).