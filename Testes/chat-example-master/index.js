var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on("connection", function (socket) {
    var tweet = "Hello, world!";

    // to make things interesting, have it send every second
    var interval = setInterval(function () {
        socket.emit("chat message", tweet);
    }, 1000);

    socket.on("disconnect", function () {
        clearInterval(interval);
    });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
