app.controller("RobotAppController", function($scope, $http, $interval) {
	$scope.lat = 0.0;
	$scope.lng = 0.0;
	
	$scope.frente = function (){
		$http.get("https://raspberrypi:3000/frente").then(function(response){
			$scope.teste=response.data;
		});
	}
	$scope.tras = function (){
		$http.get("https://raspberrypi:3000/tras").then(function(response){
			$scope.teste=response.data;
		});
	}
	$scope.esquerda = function (){
		$http.get("https://raspberrypi:3000/esquerda").then(function(response){
			$scope.teste=response.data;
		});
	}
	$scope.direita = function (){
		$http.get("https://raspberrypi:3000/direita").then(function(response){
			//$scope.teste=response.data;
		});
	}
	$scope.parar = function (){
		$http.get("https://raspberrypi:3000/parar").then(function(response){
			$scope.teste=response.data;
		});
	}
	$scope.latitude = function (){
		$scope.lat = 40.925069;
		console.log("latitude");
		$http.get("https://raspberrypi:3000/latitude").then(function(response){
			$scope.teste=response.data;
									
		});
	}
	$scope.longitude = function (){
		console.log("longitude");
		$scope.lng = -8.551720;
		$http.get("https://raspberrypi:3000/longitude").then(function(response){
			$scope.teste=response.data;
			
		});
	}
	$scope.navegarTeclas = function ($event){
		switch (event.keyCode) {
			case 38:
				$scope.frente();
			break;
			case 40:
				$scope.tras();
			break;
			case 37:
				$scope.esquerda();
			break;
			case 39:
				$scope.direita();
			break;
			case 32:
				$scope.parar();
			break;
			default:
		}
	}
	
	/* $interval(function () {
		$scope.latitude();
		$scope.longitude();
	}, 1000); */
});