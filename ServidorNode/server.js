var fs = require('fs');
var http = require('http');
var https = require('https');
var app = require('./config/express')();

//Carregamento da chave
var privateKey  = fs.readFileSync('config/certificados/robot.key', 'utf8');
//Carregamento do certificado
var certificate = fs.readFileSync('config/certificados/robot.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

https.createServer(credentials, app).listen(app.get('httpsPort'),function () {
    console.log('HTTPS Express server na porta ' + app.get('httpsPort')); 
});

http.createServer(app).listen(app.get('httpPort'),function () {
    console.log('HTTP Express server na porta ' + app.get('httpPort')); 
});

