var controller = require('../controllers/frente');

module.exports = function(app) {

    app.get('/frente', controller.frente);
    app.get('/front', controller.frente);

};