var controller = require('../controllers/camera');

module.exports = function(app) {

    app.get('/camera', controller.camera);
    app.get('/cam', controller.camera);

};