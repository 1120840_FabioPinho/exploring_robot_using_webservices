// Libraries
#include <SPI.h>
#include <aREST.h>
#include <avr/wdt.h>

// Create aREST instance
aREST rest = aREST();

int angulo = 45;
int aceleracao = 0;
bool parado = true;

void setup(void) {
  //Inicializacao da comunicacao serial
  Serial.begin(115200);

  //Inicializacao dos pinos que controlam os motores
  //Canal A
  pinMode(12, OUTPUT); //Pino da direcao
  pinMode(9, OUTPUT);  //Pino do travao

  //Canal B
  pinMode(13, OUTPUT); //Pino da direcao
  pinMode(8, OUTPUT);  //Pino do travao


  //Declaracao das funcoes disponiveis por REST
  rest.function("front", frente);
  rest.function("back", tras);
  rest.function("left", esquerda);
  rest.function("right", direita);
  rest.function("stop", parar);

  //Configuracao de dados do hardware
  rest.set_id("1");
  rest.set_name("Controlo Movimento");

  //Inicio do watchdog
  wdt_enable(WDTO_4S);
}

void frente(String params) {
  if (params.length() != 0) {
    int numParams = 0;
    getNumParametros(params, numParams);
    if(numParams == 1){
      bool sucesso = getParametro(params, false);
      if(sucesso){
        digitalWrite(12, LOW);
        digitalWrite(9, LOW);
        analogWrite(3, aceleracao);
        
        digitalWrite(13, HIGH);
        digitalWrite(8, LOW);
        analogWrite(11, aceleracao);

        parado = false;
      }
    }
  }
}

void tras(String params) {
  if (params.length() != 0) {
    int numParams = 0;
    getNumParametros(params, numParams);
    if(numParams == 1){
      bool sucesso = getParametro(params, false);
      if(sucesso){
          digitalWrite(12, HIGH);
          digitalWrite(9, LOW);
          analogWrite(3, aceleracao);
  
          digitalWrite(13, LOW);
          digitalWrite(8, LOW);
          analogWrite(11, aceleracao);

          parado = false;
        }
      }
    }
}

void direita(String params) {
  if (params.length() != 0) {
    int numParams = 0;
    getNumParametros(params, numParams);
    if(numParams == 2){
      bool sucesso = getParametro(params, true);
      if(sucesso){
        if(parado){
          digitalWrite(12, HIGH);
          digitalWrite(9, LOW);
          analogWrite(3, aceleracao);
          
          digitalWrite(13, HIGH);
          digitalWrite(8, LOW);
          analogWrite(11, aceleracao);
          parado = false;
        }
        else{
          int velocidade = calculaVelocidadeMotor(angulo, aceleracao);
          if(velocidade > 0){
            digitalWrite(12, LOW);
            digitalWrite(9, LOW);
            analogWrite(3, velocidade);
          }
          else{
            digitalWrite(12, HIGH);
            digitalWrite(9, LOW);
            analogWrite(3, (velocidade * -1));
          }
          
          digitalWrite(13, HIGH);
          digitalWrite(8, LOW);
          analogWrite(11, aceleracao);
        }
      }
    }
  }
}

void esquerda(String params) {
  if (params.length() != 0) {
    int numParams = 0;
    getNumParametros(params, numParams);
    if(numParams == 2){
      bool sucesso = getParametro(params, true);
      if(sucesso){
        if(parado){
          digitalWrite(12, LOW);
          digitalWrite(9, LOW);
          analogWrite(3, aceleracao);
          
          digitalWrite(13, LOW);
          digitalWrite(8, LOW);
          analogWrite(11, aceleracao);
          parado = false;
        }
        else{
          int velocidade = calculaVelocidadeMotor(angulo, aceleracao);
          digitalWrite(12, LOW);
          digitalWrite(9, LOW);
          analogWrite(3, aceleracao);

          if(velocidade > 0){
            digitalWrite(13, HIGH);
            digitalWrite(8, LOW);
            analogWrite(11, velocidade);
          }
          else{
            digitalWrite(13, LOW);
            digitalWrite(8, LOW);
            analogWrite(11, (velocidade * -1));
          }
        }
      }
    }
  }
}

void parar() {
  digitalWrite(9, HIGH);
  digitalWrite(8, HIGH);
  parado = true;
}

//Funcoes auxiliares para extrair parametros
int calculaVelocidadeMotor(int fAngulo, int fVelocidade){
  Serial.print("Angulo= ");
  Serial.print(fAngulo);
  Serial.print("\n");
  float retorno = 0;
  float velocidadePorGrauAngulo = fVelocidade / 45;
  if(angulo >0 && angulo < 45){
    Serial.println("angulo entre 0 e 45");
    float velocidadeDesconto = velocidadePorGrauAngulo * fAngulo;
    retorno = fVelocidade - velocidadeDesconto;
    return retorno;
  }
  else{
    if(angulo > 45 && angulo < 90){
      Serial.println("angulo maior que 45 e menor que 90");
      int angEquivalente = 45 - (angulo - 45);
      float velocidadeDesconto = velocidadePorGrauAngulo * angEquivalente;
      retorno = (fVelocidade - velocidadeDesconto) * -1;
      return retorno;
    }
  }
  return retorno;
}
//retira o nr de paramatros esperados e retira a iformacao do url recebido
int getNumParametros(String& url,int& numParams){
  numParams = url.toInt();
  int pos = find(url,'&');
  url.remove(0, pos+1);
  return numParams;
}

bool getParametro(String& params, bool curvar){
  size_t pos;
  String token;
  bool bSpeed = false;
  bool bAngle = false;
  
  do {
    pos = find(params, '&');
    token = params.substring(0, pos);
    int tPos = find(token, '=');
    if (strncmp("speed", token.substring(0, tPos).c_str(), 5) == 0) {
      if (token.substring(tPos + 1, token.length()).toInt() >= 0 && token.substring(tPos + 1, token.length()).toInt() <= 255) {
        aceleracao = token.substring(tPos + 1, token.length()).toInt();
        bSpeed = true;
      }
    }
    else{
      if (strncmp("angle", token.substring(0, tPos).c_str(), 5) == 0) {
        if (token.substring(tPos + 1, token.length()).toInt() > 0 || token.substring(tPos + 1, token.length()).toInt() < 60) {
          angulo = token.substring(tPos + 1, token.length()).toInt();
          bAngle = true;
        }
      }
    }
    params.remove(0, pos + 1);
  } while (params.length() != 0);
  if(curvar){
    return (bSpeed & bAngle);
  }
  else{
    return (bSpeed);
  }
}

int find(String text, char delimiter) {
  for (int i = 0; i < text.length(); i++) {
    if (text[i] == delimiter) {
      return i;
    }
    if (i == text.length() - 1) {
      return i + 1;
    }
  }
  return 0;
}

void loop() {
  
  //Processamento dos pedidos REST
  rest.handle(Serial);
  wdt_reset();

}
