// Libraries
#include <SPI.h>
#include <aREST.h>
#include <avr/wdt.h>
#include <Servo.h>
#include <Regexp.h>

// objetos globais
aREST rest = aREST();

Servo myservo;

int spdMotorA = 0;
int spdMotorB = 0;
int angCamera = 90;
bool left = false;
bool right = false;


void setup(void) {
  //Inicializacao da comunicacao serial
  Serial.begin(115200);

  //Inicializacao dos pinos que controlam os motores
  //Canal A
  pinMode(12, OUTPUT); //Pino da direcao
  pinMode(9, OUTPUT);  //Pino do travao

  //Canal B
  pinMode(13, OUTPUT); //Pino da direcao
  pinMode(8, OUTPUT);  //Pino do travao

  myservo.attach(6);
  myservo.write(angCamera);

  //Declaracao das funcoes disponiveis por REST
  rest.function("front", frente);
  rest.function("back", tras);
  rest.function("rotate", rodar);
  rest.function("stop", parar);
  rest.function("servo", servo);

  //Configuracao de dados do hardware
  rest.set_id("1");
  rest.set_name("Controlo Movimento");

  //Inicio do watchdog
  wdt_enable(WDTO_4S);
}

void frente(String parametros) {
  getParametros(parametros);
  digitalWrite(12, LOW);
  digitalWrite(9, LOW);
  analogWrite(3, spdMotorA);
  
  digitalWrite(13, HIGH);
  digitalWrite(8, LOW);
  analogWrite(11, spdMotorB);
}

void rodar(String parametros) {
  getParametros(parametros);
  if(left){  
    digitalWrite(12, LOW);
    digitalWrite(9, LOW);
    analogWrite(3, spdMotorA);
    
    digitalWrite(13, LOW);
    digitalWrite(8, LOW);
    analogWrite(11, spdMotorB);
  }
  if(right){
    digitalWrite(12, HIGH);
    digitalWrite(9, LOW);
    analogWrite(3, spdMotorA);
    
    digitalWrite(13, HIGH);
    digitalWrite(8, LOW);
    analogWrite(11, spdMotorB);
  }
}

void tras(String parametros) {
  getParametros(parametros);
  digitalWrite(12, HIGH);
  digitalWrite(9, LOW);
  analogWrite(3, spdMotorA);

  digitalWrite(13, LOW);
  digitalWrite(8, LOW);
  analogWrite(11, spdMotorB);
}

void parar() {
  digitalWrite(9, HIGH);
  digitalWrite(8, HIGH);

  spdMotorA = 0;
  spdMotorB = 0;
}

void servo(String parametros){
  getParametros(parametros);
  myservo.write(angCamera);
}

void getParametros(String input){
  char buf [100];
  input.toCharArray(buf,input.length()+1);

  // match state object
  MatchState ms (buf);
  
  ms.GlobalMatch ("(%a+)=(%d+)", getValores);
  ms.GlobalMatch ("(%a+)=(%a+)", getValores);
}

void getValores  (const char* match, const unsigned int length, const MatchState & ms){
  char cap [15];   
  bool motorA = false;
  bool motorB = false;
  bool camAng = false;
  bool diretion = false;
  
  for (byte i = 0; i < ms.level; i++){
    ms.GetCapture (cap, i);
    String str(cap);
    
    if(motorA){
      int val = atoi(cap);
      if(val>=100 && val <= 255 ){
        spdMotorA = val;
      }
    }
    if(motorB){
      int val = atoi(cap);
      if(val>=100 && val <= 255){
        spdMotorB = val;
      }
    }
    if(camAng){
      int val = atoi(cap);
      if(val>=0 && val <= 180){
        angCamera = val;
      }
    }
    if(diretion){
      if(str == "left"){
        left = true;
        right = false;
      }
      if(str == "right"){
        left = false;
        right = true;
      }
    }
    if(str == "motorA"){
      motorA = true;
      motorB = false;
      camAng = false;
      diretion = false;
    }
    if(str =="motorB"){
      motorA = false;
      motorB = true;
      camAng = false;
      diretion = false;
    }
    if(str == "camAng"){
      motorA = false;
      motorB = false;
      camAng = true;
      diretion = false;
    }
    if(str == "direction"){
      motorA = false;
      motorB = false;
      camAng = false;
      diretion = true;
    }
  }
}

void loop() {
  
  //Processamento dos pedidos REST
  rest.handle(Serial);
  wdt_reset();

}
