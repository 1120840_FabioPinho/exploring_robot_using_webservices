#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Ultrasonic.h>
#include <Encoder.h>

#define raio 3
#define UltraSomTrigPin 7
#define UltraSomEchoPin 8
#define GPSRxPin 13
#define GPSTxPin 3
#define encoderAA 9
#define encoderAB 10
#define encoderBA 11
#define encoderBB 12

Encoder encoderA (encoderAB, encoderAA);
Encoder encoderB (encoderBB, encoderBA);

long impulsos = 0;
long impulsosMemoria = 0;
float distanciaObjeto, distanciaPercorrida, velocidadeRobot;
static const uint32_t GPSBaud = 4800;

Ultrasonic ultrasonic(UltraSomTrigPin, UltraSomEchoPin);

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(GPSRxPin, GPSTxPin);

void GPS(){
  smartDelay(500);
}

void outputDados(){
  Serial.print("{\"distanciaObjeto\":");
  Serial.print(distanciaObjeto);
  Serial.print(",\"GPSValido\":");
  Serial.print(gps.location.isValid());
  Serial.print(",\"latitude\":");
  Serial.print(gps.location.lat(), 6);
  Serial.print(",\"longitude\":");
  Serial.print(gps.location.lng(), 6);
  Serial.print(",\"velocidadeGPS\":");
  Serial.print(gps.speed.kmph());
  Serial.print(",\"distaciaPercorrida\":");
  Serial.print(distanciaPercorrida);
  Serial.print(",\"velocidadeRobot\":");
  Serial.print(velocidadeRobot);
  Serial.print("}\n");
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms){
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

void ultrassonico(){
  long microsec = ultrasonic.timing();
  distanciaObjeto = ultrasonic.convert(microsec, Ultrasonic::CM);
  delay(150);
}

void distanciaVelocidadeEncoders(){
  //speed in 100ms
  unsigned long start = millis();
  int ms = 100;
  do{
    impulsos = encoderA.read();
  } while (millis() - start < ms);
  float perimetroRoda = 2 * M_PI * raio;
  int impulsosCiclo =  abs(impulsos - impulsosMemoria);
  impulsosMemoria = impulsos;
  //speed in seconds
  float nrVoltas = (float) (impulsosCiclo * 10 * 3) / 1000;
  velocidadeRobot = nrVoltas * perimetroRoda;
  distanciaPercorrida += velocidadeRobot;

}

void setup() {
  Serial.begin (115200);

  ss.begin(GPSBaud);
}

int firstRunGPS, firstRunEncoders, firstRunUltrassonico, firstRunOutput;
long itGPS=0, itEncoders=0, itUltrassonico=0, itOutput=0;

bool agenda(int firstTime, long sysTime, long iteration , int interval){
  int teste = ((sysTime - firstTime) / iteration);
  return (teste <= (interval+10));
}

void loop() {
  //Ultrassonico
  if(itUltrassonico == 0){
    firstRunUltrassonico = millis();
    ultrassonico();
    itUltrassonico ++;
  }
  else{
    if(agenda(firstRunUltrassonico, millis(), itUltrassonico,1000)) ultrassonico();
    itUltrassonico ++;
  }

  //Encoders
  if(itEncoders == 0){
    firstRunEncoders = millis();
    distanciaVelocidadeEncoders();
    itEncoders ++;
  }
  else{
    if(agenda(firstRunEncoders, millis(), itEncoders,1000)) distanciaVelocidadeEncoders();
    itEncoders ++;
  }

  //GPS
  if(itGPS == 0){
    firstRunGPS = millis();
    GPS();
    itGPS ++;
  }
  else{
    if(agenda(firstRunGPS, millis(), itGPS,1000)) GPS();
    itGPS ++;
  }
  
  //Output
  if(itOutput == 0){
    firstRunOutput = millis();
    outputDados();
    itGPS ++;
  }
  else{
    if(agenda(firstRunOutput, millis(), itOutput,1000)) outputDados();
    itOutput ++;
  }
}
