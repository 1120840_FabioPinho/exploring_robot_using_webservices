from modelos.VideoStream import VideoStream
import time
import datetime
import cv2

class RespostaServidorCamera:
    camInUse = False
    def __init__(self):
       pass 
    
    def cameraStart(self):
        if(self.camInUse == False):
            ## Inicializacao da camera
            self.camera = VideoStream(usePiCamera=True, resolution=(1280, 720)).start()
            time.sleep(2)
            self.camInUse = True
            pass
        else:
            pass

    def streamCamera(self):
        ## Procede a captura da frame
        frame = self.camera.read()
        frame = cv2.flip(frame,1)
        frame = cv2.flip(frame,0)
        
        ## Executa todos os tratamentos necessarios
        frameComInfo = self.printDateTimeInfo(frame)
        
        ## Converte a imagem de RAW para JPG
        r,buf = cv2.imencode(".jpg",frameComInfo)
        return buf
    
    def stopStream(self):
        self.camera.stop()
        self.camInUse = False

    def printDateTimeInfo(self,frame):
        '''
            Obtem a data e hora do sistema e coloca na frame que recebe por parametro
        '''
        timestamp = datetime.datetime.now()
        ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
        cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
        return frame
