from BaseHTTPServer import BaseHTTPRequestHandler
import time
import json
import serial

class RespostaServidorControlo(BaseHTTPRequestHandler):
    serial1 = None
    serial2 = None
    
    def __init__(self):
        serial_speed1 = 115200
        serial_port1 = '/dev/ttyACM0'
        self.serial1 = serial.Serial(serial_port1, serial_speed1, timeout=1)
        time.sleep(2)

        serial_speed2 = 115200
        serial_port2 = '/dev/ttyUSB0'
        self.serial2 = serial.Serial(serial_port2, serial_speed2, timeout=1)
        time.sleep(2)

    def do_GET(self):
        print(self.path)
        if self.path == '/frente':
            ##  faz parse do url

            ##  Envia comando para o arduino
            self.serial1.write("/front?params=1&speed=255\r")

            ##  avalia a resposta e monta resposta para aplicacao
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Access-Control-Allow-Origin","*")
            self.send_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin")
            self.send_header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept")
            self.end_headers()
            self.wfile.write("frente")
            
        elif self.path == '/tras':
            self.serial1.write("/back\r")
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Access-Control-Allow-Origin","*")
            self.send_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin")
            self.send_header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept")
            self.end_headers()
            self.wfile.write("tras")
          
        elif self.path == '/esquerda':
            self.serial1.write("/left\r")
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Access-Control-Allow-Origin","*")
            self.send_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin")
            self.send_header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept")
            self.end_headers()
            self.wfile.write("esquerda")
    
        elif self.path == '/direita':
            self.serial1.write("/right\r")
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Access-Control-Allow-Origin","*")
            self.send_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin")
            self.send_header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept")
            self.end_headers()
            self.wfile.write("direita")
            
        elif self.path == '/parar':
            self.serial1.write("/stop\r")
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.send_header("Access-Control-Allow-Origin","*")
            self.send_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin")
            self.send_header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept")
            self.end_headers()
            self.wfile.write("parado")
            
        elif self.path == '/longitude':
            self.serial2.write("/longitude\r")
            resposta = json.loads(self.serial2.readline())
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(resposta)
            
        elif self.path == '/latitude':
            self.serial2.write("/latitude\r")
            time.sleep(2)
            resposta = json.loads(self.serial2.readline())
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(resposta)
                
        elif self.path == '/favicon.ico':
          self.send_response(200)
    
        else:
          self.send_response(404)
          self.end_headers()
              
        return
