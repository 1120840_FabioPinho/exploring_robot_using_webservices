from BaseHTTPServer import BaseHTTPRequestHandler
from RespostaServidorCamera import RespostaServidorCamera
from RespostaServidorControlo import RespostaServidorControlo
import json


class RespostaServidor(BaseHTTPRequestHandler):
    camServer = RespostaServidorCamera()
    robotServer = RespostaServidorControlo()
    speed = 0
    angle = 0
    
    def __init__(self,request, client_address, server):
        ##try:
        BaseHTTPRequestHandler.__init__(self, request, client_address, server)
        ##except:
            ##print("Excecao socket apanhada")
            ##pass

    def _setHeaders(self, codigo = 404, chave = 'Content-type', valor = 'text/html'):
        self.send_response(codigo)
        self.send_header(chave, valor)
        self.end_headers()

    def _setResponse(self, response = "O recurso solicitado nao foi encontrado"):
        self.wfile.write("<html><body><h1>" + response + "</h1></body></html>")

    def _getPostRequestBody(self):
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body)
        return data

    def _setVarValues(self, json):
        try:
            self.speed = json['speed']
        except KeyError:
            pass
        try:
            self.angle = json['angle']
        except KeyError:
             pass   

    def _processResponse(self, response):
        ##valida a resposta
        self._setHeaders(200)
        self._setResponse("working on it :)")
        
    def do_GET(self):
        if self.path =="/camera":
            self._setHeaders(200, 'Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
            while True:
                frame = self.camServer.streamCamera()
                self.wfile.write("--jpgboundary\r\n")
                self.send_header('Content-type','image/jpeg')
                self.send_header('Content-length',str(len(frame)))
                self.end_headers()
                self.wfile.write(bytearray(frame))
                self.wfile.write('\r\n')
        else:
            pass

    def do_POST(self):
        print("POST request")
        
        if self.path =="/frente":
            json = self._getPostRequestBody()
            self._setVarValues(json)
            ##response = self.robotServer.request("front", self.speed)
            response = "bla"
            self._processResponse(response)
        elif self.path =="/tras":
            json = self._getPostRequestBody()
            self._setVarValues(json)
            response = self.robotServer.request("back", self.speed)
            self._processResponse(response)
        elif self.path =="/esquerda":
            json = self._getPostRequestBody()
            self._setVarValues(json)
            response = self.robotServer.request("left", self.speed, self.angle)
            self._processResponse(response)
        elif self.path =="/direita":
            json = self._getPostRequestBody()
            self._setVarValues(json)
            response = self.robotServer.request("right", self.speed, self.angle)
            self._processResponse(response)
        elif self.path =="/parar":
            pass
        else:
            self._setHeaders()
            self._setResponse()
