from BaseHTTPServer import HTTPServer
from controllers.RespostaServidor import RespostaServidor
import ssl
from threading import Thread

class Servidor(HTTPServer):
    clients = 0
    def process_request(self, request, client_address):
        thread = Thread(target=self.new_request, args=(self.RequestHandlerClass, request, client_address, self))
        thread.start()

    def new_request(self, handlerClass, request, address, server):
        handlerClass(request, address, server)
        self.shutdown_request(request)

def main():
    try:
        servidor = Servidor(('',3001),RespostaServidor)
        servidor.socket = ssl.wrap_socket(servidor.socket, "certificados/key.pem", "certificados/cert.pem", server_side = True)
        print ("server started")
        servidor.serve_forever()
    except KeyboardInterrupt:
        servidor.socket.close()
    return

if __name__ == '__main__':
    main()
