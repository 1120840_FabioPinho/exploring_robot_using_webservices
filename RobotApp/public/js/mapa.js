var pos = {
	lat: -34.397,
	lng: 150.644
	};
	
function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: pos,
		zoom: 17
	});
    	
	var infoWindow = new google.maps.InfoWindow({map: map});
	
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
            };
			
			var marker = new google.maps.Marker({
				position: pos,
				map: map
			});
			//infoWindow.setPosition(pos);
			//infoWindow.setContent("Aqui");
			map.setCenter(pos);
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
		'Error: The Geolocation service failed.' :
		'Error: Your browser doesn\'t support geolocation.');
}

/*var num = 0;
//a cada segundo
setInterval(updateMarker,1000);

function updateMarker() {
	console.log(pos.lat);
}*/
