var controller = require('../controllers/esquerda');

module.exports = function(app) {

    app.get('/esquerda', controller.esquerda);
    app.get('/left', controller.esquerda);

};