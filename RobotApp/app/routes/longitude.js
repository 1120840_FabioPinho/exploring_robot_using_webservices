var controller = require('../controllers/longitude');

module.exports = function(app) {

    app.get('/longitude', controller.longitude);
    app.get('/long', controller.longitude);

};