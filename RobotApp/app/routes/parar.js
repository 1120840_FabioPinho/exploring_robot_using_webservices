var controller = require('../controllers/parar');

module.exports = function(app) {

    app.get('/parar', controller.parar);
    app.get('/stop', controller.parar);

};