var controller = require('../controllers/latitude');

module.exports = function(app) {

    app.get('/latitude', controller.latitude);
    app.get('/lat', controller.latitude);

};