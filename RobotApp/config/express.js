var express = require('express');

var direita = require('../app/routes/direita');
var esquerda = require('../app/routes/esquerda');
var frente = require('../app/routes/frente');
var parar = require('../app/routes/parar');
var tras = require('../app/routes/tras');
var camera = require('../app/routes/camera');
//var latitude = require('../app/routes/latitude');
//var longitude = require('../app/routes/longitude');

module.exports = function(){
	console.log("bla bla bla");
    var app = express();
	app.set('httpsPort',3000);
    app.set('httpPort',3003);
    app.use(express.static('./public'));
	
	direita(app);
	esquerda(app);
	frente(app);
	parar(app);
	tras(app);
	camera(app);
	
//	latitude(app);
//	longitude(app);
	
    return app;
};